from colour.utils import hash_or_str


def test_hash_or_str() -> None:
    assert hash_or_str({"a": 1}) == "dict{'a': 1}"
    assert hash_or_str("foo") != hash_or_str("bar")

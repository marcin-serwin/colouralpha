import math

from colour.color import Color


def test_static_constructors() -> None:
    assert Color.from_hex("#bedead").hex_l == "#bedead"
    assert Color.from_web("pink").web == "pink"
    assert all(
        math.isclose(a, b)
        for a, b in zip(Color.from_rgb((0.2, 0.3, 0.4)).rgb, (0.2, 0.3, 0.4))
    )
    assert all(
        math.isclose(a, b)
        for a, b in zip(Color.from_hsl((0.2, 0.3, 0.4)).hsl, (0.2, 0.3, 0.4))
    )

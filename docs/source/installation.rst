Installation
============

You don't need to download the GIT version of the code as ``colouralpha`` is
available on the PyPI. So you should be able to run:

.. code-block:: console

    $ pip install colouralpha

If you have downloaded the GIT sources, then you could add the ``colour.py``
directly to one of your ``site-packages`` (thanks to a symlink). Or install
the current version via:

.. code-block:: console

    $ poetry install

And if you don't have the GIT sources but would like to get the latest
main or branch from gitlab, you could also:

.. code-block:: console

    $ pip install git+https://gitlab.com/marcin-serwin/colouralpha.git

Or even select a specific revision (branch/tag/commit):

.. code-block:: console

    $ pip install git+https://gitlab.com/marcin-serwin/colouralpha.git@main
    $ ls
    $ dir

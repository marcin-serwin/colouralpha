.. ColourAlpha documentation master file, created by
   sphinx-quickstart on Tue Jan  4 09:10:42 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ColourAlpha's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   usage
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

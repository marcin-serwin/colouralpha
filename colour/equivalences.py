"""Functions to compare colors."""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .color import Color


def RGB_equivalence(c1: Color, c2: Color) -> bool:
    """Compare whether two colors are equivalent according to their RGB values."""
    return c1.hex_l == c2.hex_l


def HSL_equivalence(c1: Color, c2: Color) -> bool:
    """Compare whether two colors are equivalent according to their HSL values."""
    return c1._hsl == c2._hsl

"""Color Library.

.. :doctest:

This module defines several color formats that can be converted to one or
another.

Formats
-------

HSL:
    3-uple of Hue, Saturation, Lightness all between 0.0 and 1.0

RGB:
    3-uple of Red, Green, Blue all between 0.0 and 1.0

HEX:
    string object beginning with '#' and with red, green, blue value.
    This format accept color in 3 or 6 value ex: '#fff' or '#ffffff'

WEB:
    string object that defaults to HEX representation or human if possible

Usage
-----

Several function exists to convert from one format to another. But all
function are not written. So the best way is to use the object Color.

Please see the documentation of this object for more information.

.. note:: Some constants are defined for convenience in HSL, RGB, HEX
"""
from __future__ import annotations

import hashlib
from typing import Any, Callable, Iterator, cast

from ._typealiases import _Hex, _Hsl, _Rgb, _Web
from .constants import COLOR_NAME_TO_RGB
from .conversions import hex2rgb, hex2web, hsl2rgb, rgb2hex, rgb2hsl, web2hex
from .equivalences import RGB_equivalence
from .utils import color_scale, hash_or_str


class C_HSL:
    """HSL colors container.

    Provides a quick color access.

    >>> from colour import HSL

    >>> HSL.WHITE
    (0.0, 0.0, 1.0)
    >>> HSL.BLUE # doctest: +ELLIPSIS
    (0.66..., 1.0, 0.5)

    >>> HSL.DONOTEXISTS  # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    AttributeError: ... has no attribute 'DONOTEXISTS'
    """

    def __getattr__(self, value: str) -> _Hsl:
        """Get hsl value of a color by its name."""
        label = value.lower()
        if label in COLOR_NAME_TO_RGB:
            return rgb2hsl(
                cast(
                    _Hsl,
                    tuple(v / 255.0 for v in COLOR_NAME_TO_RGB[label]),
                )
            )
        raise AttributeError(f"{self.__class__} instance has no attribute {value!r}")


HSL = C_HSL()


class C_RGB:
    """RGB colors container.

    Provides a quick color access.

    >>> from colour import RGB

    >>> RGB.WHITE
    (1.0, 1.0, 1.0)
    >>> RGB.BLUE
    (0.0, 0.0, 1.0)

    >>> RGB.DONOTEXISTS  # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    AttributeError: ... has no attribute 'DONOTEXISTS'
    """

    def __getattr__(self, value: str) -> _Rgb:
        """Get rgb value of a color by its name."""
        return hsl2rgb(getattr(HSL, value))


class C_HEX:
    """RGB colors container.

    Provides a quick color access.

    >>> from colour import HEX

    >>> HEX.WHITE
    '#fff'
    >>> HEX.BLUE
    '#00f'

    >>> HEX.DONOTEXISTS  # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    AttributeError: ... has no attribute 'DONOTEXISTS'
    """

    def __getattr__(self, value: str) -> _Hex:
        """Get hex value of a color by its name."""
        return rgb2hex(getattr(RGB, value))


RGB = C_RGB()
HEX = C_HEX()


def RGB_color_picker(obj: object) -> Color:
    """Build a color representation from the string representation of an object.

    This allows to quickly get a color from some data, with the
    additional benefit that the color will be the same as long as the
    (string representation of the) data is the same::

        >>> from colour import RGB_color_picker, Color

    Same inputs produce the same result::

        >>> RGB_color_picker("Something") == RGB_color_picker("Something")
        True

    ... but different inputs produce different colors::

        >>> RGB_color_picker("Something") != RGB_color_picker("Something else")
        True

    In any case, we still get a ``Color`` object::

        >>> isinstance(RGB_color_picker("Something"), Color)
        True
    """
    # Turn the input into a by 3-dividable string. SHA-384 is good because it
    # divides into 3 components of the same size, which will be used to
    # represent the RGB values of the color.
    digest = hashlib.sha384(str(obj).encode("utf-8")).hexdigest()

    # Split the digest into 3 sub-strings of equivalent size.
    subsize = int(len(digest) / 3)
    splitted_digest = [digest[i * subsize : (i + 1) * subsize] for i in range(3)]

    # Convert those hexadecimal sub-strings into integer and scale them down
    # to the 0..1 range.
    max_value = float(int("f" * subsize, 16))
    components = cast(
        _Rgb,
        tuple(
            # Make a number from a list with hex digits
            # Scale it down to [0.0, 1.0]
            int(d, 16) / max_value
            for d in splitted_digest
        ),
    )

    return Color(rgb2hex(components))  # Profit!


class Color:
    """Abstraction of a color object.

    Color object keeps information of a color. It can input/output to different
    format (HSL, RGB, HEX, WEB) and their partial representation.

        >>> from colour import Color, HSL

        >>> b = Color()
        >>> b.hsl = HSL.BLUE

    Access values
    -------------

        >>> b.hue  # doctest: +ELLIPSIS
        0.66...
        >>> b.saturation
        1.0
        >>> b.luminance
        0.5

        >>> b.red
        0.0
        >>> b.blue
        1.0
        >>> b.green
        0.0

        >>> b.rgb
        (0.0, 0.0, 1.0)
        >>> b.hsl  # doctest: +ELLIPSIS
        (0.66..., 1.0, 0.5)
        >>> b.hex
        '#00f'

    Change values
    -------------

    Let's change Hue toward red tint:

        >>> b.hue = 0.0
        >>> b.hex
        '#f00'

        >>> b.hue = 2.0/3
        >>> b.hex
        '#00f'

    In the other way round:

        >>> b.hex = '#f00'
        >>> b.hsl
        (0.0, 1.0, 0.5)

    Long hex can be accessed directly:

        >>> b.hex_l = '#123456'
        >>> b.hex_l
        '#123456'
        >>> b.hex
        '#123456'

        >>> b.hex_l = '#ff0000'
        >>> b.hex_l
        '#ff0000'
        >>> b.hex
        '#f00'

    Convenience
    -----------

        >>> c = Color('blue')
        >>> c
        <Color blue>
        >>> c.hue = 0
        >>> c
        <Color red>

        >>> c.saturation = 0.0
        >>> c.hsl  # doctest: +ELLIPSIS
        (..., 0.0, 0.5)
        >>> c.rgb
        (0.5, 0.5, 0.5)
        >>> c.hex
        '#7f7f7f'
        >>> c
        <Color #7f7f7f>

        >>> c.luminance = 0.0
        >>> c
        <Color black>

        >>> c.hex
        '#000'

        >>> c.green = 1.0
        >>> c.blue = 1.0
        >>> c.hex
        '#0ff'
        >>> c
        <Color cyan>

        >>> c = Color('blue', luminance=0.75)
        >>> c
        <Color #7f7fff>

        >>> c = Color('red', red=0.5)
        >>> c
        <Color #7f0000>

        >>> print(c)
        #7f0000

    You can try to query unexisting attributes:

        >>> c.lightness  # doctest: +ELLIPSIS
        Traceback (most recent call last):
        ...
        AttributeError: 'lightness' not found

    TODO: could add HSV, CMYK, YUV conversion.

    # >>> b.hsv
    # >>> b.value
    # >>> b.cyan
    # >>> b.magenta
    # >>> b.yellow
    # >>> b.key
    # >>> b.cmyk


    Recursive init
    --------------

    To support blind conversion of web strings (or already converted object),
    the Color object supports instantiation with another Color object.

        >>> Color(Color(Color('red')))
        <Color red>

    Equality support
    ----------------

    Default equality is RGB hex comparison:

        >>> Color('red') == Color('blue')
        False
        >>> Color('red') == Color('red')
        True
        >>> Color('red') != Color('blue')
        True
        >>> Color('red') != Color('red')
        False

    But this can be changed:

        >>> saturation_equality = lambda c1, c2: c1.luminance == c2.luminance
        >>> Color('red', equality=saturation_equality) == Color('blue')
        True


    Subclassing support
    -------------------

    You should be able to subclass ``Color`` object without any issues::

        >>> class Tint(Color):
        ...     pass

    And keep the internal API working::

        >>> Tint("red").hsl
        (0.0, 1.0, 0.5)
    """

    equality = staticmethod(RGB_equivalence)

    def __init__(
        self,
        color: _Web | Color | None = None,
        pick_for: object | None = None,
        picker: Callable[[object], Color] = RGB_color_picker,
        pick_key: Callable[[object], str | int] = hash_or_str,
        **kwargs: Any,
    ) -> None:
        """Create ``Color``.

        Parameters
        ----------
        color
            The initializer of color. If left as ``None``, it defaults to black.

        pick_for
            The object to be hashed and converted into color.

        picker
            Method converting hash of an object into RGB color.

        pick_key
            Method picking a key from object to be hashed.
        """
        if pick_key is None:

            def pick_key(x: object) -> object:
                return x

        if pick_for is not None:
            color = picker(pick_key(pick_for))

        if isinstance(color, Color):
            self.web = color.web
        else:
            self.web = color if color else "black"

        for k, v in kwargs.items():
            setattr(self, k, v)

    def __getattr__(self, label: str) -> tuple[Any]:
        """No longer used. Kept for backward compatiblity.

        Previously: Transform getting attribute in the form ``color.foo`` to ``color.get_foo()`` whenever it is possible.
        """
        raise AttributeError(f"'{label}' not found")

    # HSL

    def get_hsl(self) -> _Hsl:
        """Get hsl notation of the color."""
        return (self._hsl[0], self._hsl[1], self._hsl[2])

    def set_hsl(self, value: _Hsl) -> None:
        """Set color using hsl notation."""
        self._hsl: list[float] = list(value)

    @property
    def hsl(self) -> _Hsl:
        """Get hsl notation of the color."""
        return self.get_hsl()

    @hsl.setter
    def hsl(self, hsl: _Hsl) -> None:
        self.set_hsl(hsl)

    def get_hue(self) -> float:
        """Get hue of the color."""
        return self.hsl[0]

    def set_hue(self, value: float) -> None:
        """Set hue of the color."""
        self._hsl[0] = value

    @property
    def hue(self) -> float:
        """Get hue of the color."""
        return self.get_hue()

    @hue.setter
    def hue(self, hue: float) -> None:
        self.set_hue(hue)

    def get_saturation(self) -> float:
        """Get saturation of the color."""
        return self.hsl[1]

    def set_saturation(self, value: float) -> None:
        """Set saturation of the color."""
        self._hsl[1] = value

    @property
    def saturation(self) -> float:
        """Get saturation of the color."""
        return self.get_saturation()

    @saturation.setter
    def saturation(self, saturation: float) -> None:
        self.set_saturation(saturation)

    def get_luminance(self) -> float:
        """Get luminance of the color."""
        return self.hsl[2]

    def set_luminance(self, value: float) -> None:
        """Set luminance of the color."""
        self._hsl[2] = value

    @property
    def luminance(self) -> float:
        """Get luminance of the color."""
        return self.get_luminance()

    @luminance.setter
    def luminance(self, luminance: float) -> None:
        self.set_luminance(luminance)

    # RGB

    def get_rgb(self) -> _Rgb:
        """Get rgb notation of the color."""
        return hsl2rgb(self.hsl)

    def set_rgb(self, value: _Rgb) -> None:
        """Set color using rgb notation."""
        self.hsl = rgb2hsl(value)

    @property
    def rgb(self) -> _Rgb:
        """Get rgb notation of the color."""
        return self.get_rgb()

    @rgb.setter
    def rgb(self, rgb: _Rgb) -> None:
        self.set_rgb(rgb)

    def get_red(self) -> float:
        """Get red component of the color."""
        return self.rgb[0]

    def set_red(self, value: float) -> None:
        """Set red coordinate of the color."""
        _, g, b = self.rgb
        self.rgb = (value, g, b)

    @property
    def red(self) -> float:
        """Get red of the color."""
        return self.get_red()

    @red.setter
    def red(self, red: float) -> None:
        self.set_red(red)

    def get_green(self) -> float:
        """Get green component of the color."""
        return self.rgb[1]

    def set_green(self, value: float) -> None:
        """Set green coordinate of the color."""
        r, _, b = self.rgb
        self.rgb = (r, value, b)

    @property
    def green(self) -> float:
        """Get green of the color."""
        return self.get_green()

    @green.setter
    def green(self, green: float) -> None:
        self.set_green(green)

    def get_blue(self) -> float:
        """Get blue component of the color."""
        return self.rgb[2]

    def set_blue(self, value: float) -> None:
        """Set blue coordinate of the color."""
        r, g, _ = self.rgb
        self.rgb = (r, g, value)

    @property
    def blue(self) -> float:
        """Get blue of the color."""
        return self.get_blue()

    @blue.setter
    def blue(self, blue: float) -> None:
        self.set_blue(blue)

    # Hex

    def get_hex(self) -> _Hex:
        """Get shortest possible hex notation of the color."""
        return rgb2hex(self.rgb)

    def set_hex(self, value: _Hex) -> None:
        """Set a color using hex notation."""
        self.rgb = hex2rgb(value)

    # TODO deprecate hex

    @property
    def hex(self) -> _Hex:  # noqa:A003
        """Get hex of the color."""
        return self.get_hex()

    @hex.setter  # noqa:A003
    def hex(self, hex_value: _Hex) -> None:  # noqa:A003
        self.set_hex(hex_value)

    def get_hex_l(self) -> _Hex:
        """Get shortest possible hex notation of the color."""
        return rgb2hex(self.rgb, force_long=True)

    set_hex_l = set_hex

    @property
    def hex_l(self) -> _Hex:
        """Get long hex notation of the color."""
        return self.get_hex_l()

    @hex_l.setter
    def hex_l(self, hex_l: _Hex) -> None:
        self.set_hex_l(hex_l)

    # Web

    def get_web(self) -> _Web:
        """Get web representation of the color."""
        return hex2web(self.hex)

    def set_web(self, value: _Web) -> None:
        """Set a color using web notation."""
        self.hex = web2hex(value)

    @property
    def web(self) -> _Web:
        """Get web notation of the color."""
        return self.get_web()

    @web.setter
    def web(self, web: _Web) -> None:
        self.set_web(web)

    # range of color generation

    def range_to(self, value: Color, steps: int) -> Iterator[Color]:
        """Produce a uniform range of colors.

        starting with ``self`` and ending with ``value``.
        """
        for hsl in color_scale(self.hsl, Color(value).hsl, steps - 1):
            yield Color(hsl=hsl)

    ##
    # Convenience
    ##

    def __str__(self) -> str:
        """Return a color in human readable form."""
        return str(self.web)

    def __repr__(self) -> str:
        """Return representation of color in human readable form."""
        return f"<Color {self.web}>"

    def __eq__(self, other: object) -> bool:
        """Compare two colors according to the equality comparator of the first color."""
        if isinstance(other, Color):
            return self.equality(self, other)
        return NotImplemented

    ##
    # Constructors
    ##

    @staticmethod
    def from_hsl(hsl_value: _Hsl) -> Color:
        """Return new ``Color`` object based on provided hsl vaue."""
        return Color(hsl=hsl_value)

    @staticmethod
    def from_rgb(rgb_value: _Rgb) -> Color:
        """Return new ``Color`` object based on provided rgb vaue."""
        return Color(rgb=rgb_value)

    @staticmethod
    def from_hex(hex_value: _Hex) -> Color:
        """Return new ``Color`` object based on provided hex vaue."""
        return Color(hex=hex_value)

    @staticmethod
    def from_web(web_value: _Web) -> Color:
        """Return new ``Color`` object based on provided web vaue."""
        return Color(web=web_value)


def make_color_factory(**kwargs_defaults: Any) -> Callable[..., Color]:
    """Create a factory of ``Color`` with given defaults."""

    def ColorFactory(*args: Any, **kwargs: Any) -> Color:
        new_kwargs = kwargs_defaults.copy()
        new_kwargs.update(kwargs)
        return Color(*args, **new_kwargs)

    return ColorFactory

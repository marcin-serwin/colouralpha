"""Various utilities functions."""

from __future__ import annotations

from typing import Sequence, cast

from ._typealiases import _Hsl


def color_scale(
    begin_hsl: _Hsl,
    end_hsl: _Hsl,
    nb: int,
) -> Sequence[_Hsl]:
    """Return a list of nb color HSL tuples between begin_hsl and end_hsl.

    >>> from colour import color_scale, rgb2hex, hsl2rgb

    >>> [rgb2hex(hsl2rgb(hsl)) for hsl in color_scale((0, 1, 0.5),
    ...                                               (1, 1, 0.5), 3)]
    ['#f00', '#0f0', '#00f', '#f00']

    >>> [rgb2hex(hsl2rgb(hsl))
    ...  for hsl in color_scale((0, 0, 0),
    ...                         (0, 0, 1),
    ...                         15)]  # doctest: +ELLIPSIS
    ['#000', '#111', '#222', ..., '#ccc', '#ddd', '#eee', '#fff']

    Of course, asking for negative values is not supported:

    >>> color_scale((0, 1, 0.5), (1, 1, 0.5), -2)
    Traceback (most recent call last):
    ...
    ValueError: Unsupported negative number of colors (nb=-2).
    """
    if nb < 0:
        raise ValueError(f"Unsupported negative number of colors (nb={nb!r}).")

    step: _Hsl = cast(
        _Hsl,
        tuple(float(end_hsl[i] - begin_hsl[i]) / nb for i in range(0, 3))
        if nb > 0
        else (0, 0, 0),
    )

    def mul(step: _Hsl, value: int) -> _Hsl:
        return cast(_Hsl, tuple(v * value for v in step))

    def add_v(step: _Hsl, step2: _Hsl) -> _Hsl:
        return cast(
            _Hsl,
            tuple(v + step2[i] for i, v in enumerate(step)),
        )

    return [add_v(begin_hsl, mul(step, r)) for r in range(0, nb + 1)]


def hash_or_str(obj: object) -> int | str:
    """Convert an object into a unique hash.

    Parameters
    ----------
    obj
        object to be converted
    """
    try:
        return hash((type(obj).__name__, obj))
    except TypeError:
        # Adds the type name to make sure two object of different type but
        # identical string representation get distinguished.
        return type(obj).__name__ + str(obj)

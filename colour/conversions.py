"""Conversion functions of the form ``x2y`` (e.g. ``rgb2hex``)."""
import re
from typing import Tuple, cast

from ._typealiases import _Hex, _Hsl, _Rgb, _Web
from .constants import (
    COLOR_NAME_TO_RGB,
    FLOAT_ERROR,
    LONG_HEX_COLOR,
    RGB_TO_COLOR_NAMES,
    SHORT_HEX_COLOR,
)


def hsl2rgb(hsl: _Hsl) -> _Rgb:
    """Convert HSL representation towards RGB.

    :param h: Hue, position around the chromatic circle (h=1 equiv h=0)
    :param s: Saturation, color saturation (0=full gray, 1=full color)
    :param l: Ligthness, Overhaul lightness (0=full black, 1=full white)
    :rtype: 3-uple for RGB values in float between 0 and 1

    Hue, Saturation, Range from Lightness is a float between 0 and 1

    Note that Hue can be set to any value but as it is a rotation
    around the chromatic circle, any value above 1 or below 0 can
    be expressed by a value between 0 and 1 (Note that h=0 is equiv
    to h=1).

    This algorithm came from:
    http://www.easyrgb.com/index.php?X=MATH&H=19#text19

    Here are some quick notion of HSL to RGB conversion:

    >>> from colour import hsl2rgb

    With a lightness put at 0, RGB is always rgbblack

    >>> hsl2rgb((0.0, 0.0, 0.0))
    (0.0, 0.0, 0.0)
    >>> hsl2rgb((0.5, 0.0, 0.0))
    (0.0, 0.0, 0.0)
    >>> hsl2rgb((0.5, 0.5, 0.0))
    (0.0, 0.0, 0.0)

    Same for lightness put at 1, RGB is always rgbwhite

    >>> hsl2rgb((0.0, 0.0, 1.0))
    (1.0, 1.0, 1.0)
    >>> hsl2rgb((0.5, 0.0, 1.0))
    (1.0, 1.0, 1.0)
    >>> hsl2rgb((0.5, 0.5, 1.0))
    (1.0, 1.0, 1.0)

    With saturation put at 0, the RGB should be equal to Lightness:

    >>> hsl2rgb((0.0, 0.0, 0.25))
    (0.25, 0.25, 0.25)
    >>> hsl2rgb((0.5, 0.0, 0.5))
    (0.5, 0.5, 0.5)
    >>> hsl2rgb((0.5, 0.0, 0.75))
    (0.75, 0.75, 0.75)

    With saturation put at 1, and lightness put to 0.5, we can find
    normal full red, green, blue colors:

    >>> hsl2rgb((0 , 1.0, 0.5))
    (1.0, 0.0, 0.0)
    >>> hsl2rgb((1 , 1.0, 0.5))
    (1.0, 0.0, 0.0)
    >>> hsl2rgb((1.0/3 , 1.0, 0.5))
    (0.0, 1.0, 0.0)
    >>> hsl2rgb((2.0/3 , 1.0, 0.5))
    (0.0, 0.0, 1.0)

    Of course:
    >>> hsl2rgb((0.0, 2.0, 0.5))  # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    ValueError: Saturation must be between 0 and 1.

    And:
    >>> hsl2rgb((0.0, 0.0, 1.5))  # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    ValueError: Lightness must be between 0 and 1.
    """
    hue, sat, lit = (float(v) for v in hsl)

    if not (0.0 - FLOAT_ERROR <= sat <= 1.0 + FLOAT_ERROR):
        raise ValueError("Saturation must be between 0 and 1.")
    if not (0.0 - FLOAT_ERROR <= lit <= 1.0 + FLOAT_ERROR):
        raise ValueError("Lightness must be between 0 and 1.")

    if sat == 0:
        return lit, lit, lit

    if lit < 0.5:
        v2 = lit * (1.0 + sat)
    else:
        v2 = (lit + sat) - (sat * lit)

    v1 = 2.0 * lit - v2

    red = _hue2rgb(v1, v2, hue + (1.0 / 3))
    green = _hue2rgb(v1, v2, hue)
    blue = _hue2rgb(v1, v2, hue - (1.0 / 3))

    return red, green, blue


def rgb2hsl(rgb: _Rgb) -> _Hsl:
    """Convert RGB representation towards HSL.

    :param r: Red amount (float between 0 and 1)
    :param g: Green amount (float between 0 and 1)
    :param b: Blue amount (float between 0 and 1)
    :rtype: 3-uple for HSL values in float between 0 and 1

    This algorithm came from:
    http://www.easyrgb.com/index.php?X=MATH&H=19#text19

    Here are some quick notion of RGB to HSL conversion:

    >>> from colour import rgb2hsl

    Note that if red amount is equal to green and blue, then you
    should have a gray value (from black to white).


    >>> rgb2hsl((1.0, 1.0, 1.0))  # doctest: +ELLIPSIS
    (..., 0.0, 1.0)
    >>> rgb2hsl((0.5, 0.5, 0.5))  # doctest: +ELLIPSIS
    (..., 0.0, 0.5)
    >>> rgb2hsl((0.0, 0.0, 0.0))  # doctest: +ELLIPSIS
    (..., 0.0, 0.0)

    If only one color is different from the others, it defines the
    direct Hue:

    >>> rgb2hsl((0.5, 0.5, 1.0))  # doctest: +ELLIPSIS
    (0.66..., 1.0, 0.75)
    >>> rgb2hsl((0.2, 0.1, 0.1))  # doctest: +ELLIPSIS
    (0.0, 0.33..., 0.15...)

    Having only one value set, you can check that:

    >>> rgb2hsl((1.0, 0.0, 0.0))
    (0.0, 1.0, 0.5)
    >>> rgb2hsl((0.0, 1.0, 0.0))  # doctest: +ELLIPSIS
    (0.33..., 1.0, 0.5)
    >>> rgb2hsl((0.0, 0.0, 1.0))  # doctest: +ELLIPSIS
    (0.66..., 1.0, 0.5)

    Regression check upon very close values in every component of
    red, green and blue:

    >>> rgb2hsl((0.9999999999999999, 1.0, 0.9999999999999994))
    (0.0, 0.0, 0.999...)

    Of course:

    >>> rgb2hsl((0.0, 2.0, 0.5))  # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    ValueError: Green must be between 0 and 1. You provided 2.0.

    And:
    >>> rgb2hsl((0.0, 0.0, 1.5))  # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    ValueError: Blue must be between 0 and 1. You provided 1.5.
    """
    r, g, b = (float(v) for v in rgb)

    for name, v in {"Red": r, "Green": g, "Blue": b}.items():
        if not (0 - FLOAT_ERROR <= v <= 1 + FLOAT_ERROR):
            raise ValueError(f"{name} must be between 0 and 1. You provided {v!r}.")

    vmin = min(r, g, b)  # Min. value of RGB
    vmax = max(r, g, b)  # Max. value of RGB
    diff = vmax - vmin  # Delta RGB value

    vsum = vmin + vmax

    lit = vsum / 2

    if diff < FLOAT_ERROR:  # This is a gray, no chroma...
        return (0.0, 0.0, lit)

    ##
    # Chromatic data...
    ##

    # Saturation
    if lit < 0.5:
        s = diff / vsum
    else:
        s = diff / (2.0 - vsum)

    dr = (((vmax - r) / 6) + (diff / 2)) / diff
    dg = (((vmax - g) / 6) + (diff / 2)) / diff
    db = (((vmax - b) / 6) + (diff / 2)) / diff

    if r == vmax:
        h = db - dg
    elif g == vmax:
        h = (1.0 / 3) + dr - db
    elif b == vmax:
        h = (2.0 / 3) + dg - dr

    if h < 0:
        h += 1
    if h > 1:
        h -= 1

    return (h, s, lit)


def _hue2rgb(v1: float, v2: float, vH: float) -> float:
    """Private helper function (Do not call directly).

    :param vH: rotation around the chromatic circle (between 0..1)
    """
    while vH < 0:
        vH += 1
    while vH > 1:
        vH -= 1

    if 6 * vH < 1:
        return v1 + (v2 - v1) * 6 * vH
    if 2 * vH < 1:
        return v2
    if 3 * vH < 2:
        return v1 + (v2 - v1) * ((2.0 / 3) - vH) * 6

    return v1


def rgb2hex(rgb: _Rgb, force_long: float = False) -> _Hex:
    """Transform RGB tuple to hex RGB representation.

    :param rgb: RGB 3-uple of float between 0 and 1
    :rtype: 3 hex char or 6 hex char string representation

    Usage
    -----

    >>> from colour import rgb2hex

    >>> rgb2hex((0.0,1.0,0.0))
    '#0f0'

    Rounding try to be as natural as possible:

    >>> rgb2hex((0.0,0.999999,1.0))
    '#0ff'

    And if not possible, the 6 hex char representation is used:

    >>> rgb2hex((0.23,1.0,1.0))
    '#3bffff'

    >>> rgb2hex((0.0,0.999999,1.0), force_long=True)
    '#00ffff'
    """
    hx = "".join([f"{int(c * 255 + 0.5 - FLOAT_ERROR):02x}" for c in rgb])

    if not force_long and hx[0::2] == hx[1::2]:
        hx = "".join(hx[0::2])

    return f"#{hx}"


def hex2rgb(str_rgb: _Hex) -> _Rgb:
    """Transform hex RGB representation to RGB tuple.

    :param str_rgb: 3 hex char or 6 hex char string representation
    :rtype: RGB 3-uple of float between 0 and 1

    >>> from colour import hex2rgb

    >>> hex2rgb('#00ff00')
    (0.0, 1.0, 0.0)

    >>> hex2rgb('#0f0')
    (0.0, 1.0, 0.0)

    >>> hex2rgb('#aaa')  # doctest: +ELLIPSIS
    (0.66..., 0.66..., 0.66...)

    >>> hex2rgb('#aa')  # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    ValueError: Invalid value '#aa' provided for rgb color.
    """
    rgb = str_rgb[1:]

    if len(rgb) == 6:
        r, g, b = rgb[0:2], rgb[2:4], rgb[4:6]
    elif len(rgb) == 3:
        r, g, b = rgb[0] * 2, rgb[1] * 2, rgb[2] * 2
    else:
        raise ValueError(f"Invalid value {str_rgb!r} provided for rgb color.")

    return cast(
        Tuple[float, float, float],
        tuple(float(int(v, 16)) / 255 for v in (r, g, b)),
    )


def hex2web(hex_color: _Hex) -> _Web:
    """Convert HEX representation to WEB.

    :param rgb: 3 hex char or 6 hex char string representation
    :rtype: web string representation (human readable if possible)

    WEB representation uses X11 rgb.txt to define conversion
    between RGB and english color names.

    Usage
    =====

    >>> from colour import hex2web

    >>> hex2web('#ff0000')
    'red'

    >>> hex2web('#aaaaaa')
    '#aaa'

    >>> hex2web('#abc')
    '#abc'

    >>> hex2web('#acacac')
    '#acacac'
    """
    dec_rgb = cast(
        Tuple[int, int, int],
        tuple(int(v * 255) for v in hex2rgb(hex_color)),
    )
    if dec_rgb in RGB_TO_COLOR_NAMES:
        # take the first one
        color_name = RGB_TO_COLOR_NAMES[dec_rgb][0]
        # Enforce full lowercase for single worded color name.
        return (
            color_name
            if len(re.sub(r"[^A-Z]", "", color_name)) > 1
            else color_name.lower()
        )

    # Hex format is verified by hex2rgb function. And should be 3 or 6 digit
    if (
        len(hex_color) == 7
        and hex_color[1] == hex_color[2]
        and hex_color[3] == hex_color[4]
        and hex_color[5] == hex_color[6]
    ):
        return "#" + hex_color[1] + hex_color[3] + hex_color[5]
    return hex_color


def web2hex(web: _Web, force_long: bool = False) -> _Hex:
    """Convert WEB representation to HEX.

    :param rgb: web string representation (human readable if possible)
    :rtype: 3 hex char or 6 hex char string representation

    WEB representation uses X11 rgb.txt to define conversion
    between RGB and english color names.

    Usage
    =====

    >>> from colour import web2hex, Color

    >>> web2hex('red')
    '#f00'

    >>> web2hex('#aaa')
    '#aaa'

    >>> web2hex('#foo')  # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    AttributeError: '#foo' is not in web format. Need 3 or 6 hex digit.

    >>> web2hex('#aaa', force_long=True)
    '#aaaaaa'

    >>> web2hex('#aaaaaa')
    '#aaaaaa'

    >>> web2hex('#aaaa')  # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    AttributeError: '#aaaa' is not in web format. Need 3 or 6 hex digit.

    >>> web2hex('pinky')  # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    ValueError: 'pinky' is not a recognized color.

    And color names are case insensitive:

    >>> Color('RED')
    <Color red>
    """
    if web.startswith("#"):
        if LONG_HEX_COLOR.match(web) or (not force_long and SHORT_HEX_COLOR.match(web)):
            return web.lower()
        elif SHORT_HEX_COLOR.match(web) and force_long:
            return "#" + "".join([(f"{t}") * 2 for t in web[1:]])
        raise AttributeError(f"{web!r} is not in web format. Need 3 or 6 hex digit.")

    web = web.lower()
    if web not in COLOR_NAME_TO_RGB:
        raise ValueError(f"{web!r} is not a recognized color.")

    # convert dec to hex:

    return rgb2hex(
        cast(
            Tuple[float, float, float],
            tuple(float(int(v)) / 255 for v in COLOR_NAME_TO_RGB[web]),
        ),
        force_long,
    )


# Missing functions conversion


def hsl2hex(x: _Hsl) -> _Hex:
    """Convert color from hsl to hex."""
    return rgb2hex(hsl2rgb(x))


def hex2hsl(x: _Hex) -> _Hsl:
    """Convert color from hex to hsl."""
    return rgb2hsl(hex2rgb(x))


def rgb2web(x: _Rgb) -> _Web:
    """Convert color from rgb to web."""
    return hex2web(rgb2hex(x))


def web2rgb(x: _Web) -> _Rgb:
    """Convert color from web to rgb."""
    return hex2rgb(web2hex(x))


def web2hsl(x: _Web) -> _Hsl:
    """Convert color from web to hsl."""
    return rgb2hsl(web2rgb(x))


def hsl2web(x: _Hsl) -> _Web:
    """Convert color from hsl to web."""
    return rgb2web(hsl2rgb(x))

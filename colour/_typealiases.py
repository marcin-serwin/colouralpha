from typing import Tuple

_Hsl = Tuple[float, float, float]
_Rgb = Tuple[float, float, float]
_Hex = str
_Web = str

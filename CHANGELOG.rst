=========
Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_, and this project adheres
to `Semantic Versioning`_.

`Unreleased`_
=============

`0.1.6`_ - 2022-01-06
=======================

Added
-----

-  Docstrings for all public methods
-  Type annotations to all methods
-  ``from_hsl``, ``from_rgb``, ``from_hex``, ``from_web`` static methods
-  ``equality`` is now class attribute

`0.1.5+1`_ - 2022-01-04
=======================

This update swaps setuptools for poetry and such. Functionally it should be
identical to the ``0.1.5`` of the original Colour.

.. _Unreleased: https://gitlab.com/marcin-serwin/colouralpha/-/compare/v0.1.6...main
.. _0.1.6: https://gitlab.com/marcin-serwin/colouralpha/-/tags/v0.1.6
.. _0.1.5+1: https://gitlab.com/marcin-serwin/colouralpha/-/tags/v0.1.5+1
.. _Semantic Versioning: https://semver.org/spec/v2.0.0.html
.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/

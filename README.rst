===========
ColourAlpha
===========

.. image:: https://img.shields.io/pypi/v/colouralpha.svg?style=flat-square
   :target: https://pypi.python.org/pypi/colouralpha/
   :alt: Latest PyPI version

.. image:: https://img.shields.io/pypi/l/colouralpha.svg?style=flat-square
   :target: https://gitlab.com/marcin-serwin/colouralpha/-/blob/main/LICENSE
   :alt: License

.. image:: https://img.shields.io/pypi/pyversions/colouralpha.svg?style=flat-square
   :target: https://pypi.python.org/pypi/colouralpha/
   :alt: Compatible python versions

.. image:: https://img.shields.io/pypi/dm/colouralpha.svg?style=flat-square
   :target: https://pypi.python.org/pypi/colouralpha/
   :alt: Number of PyPI downloads

.. image:: https://gitlab.com/marcin-serwin/colouralpha/badges/main/pipeline.svg?style=flat-square
   :target: https://gitlab.com/marcin-serwin/colouralpha/-/pipelines
   :alt: GitLab pipeline status

.. image:: https://gitlab.com/marcin-serwin/colouralpha/badges/main/coverage.svg?style=flat-square
   :target: https://gitlab.com/marcin-serwin/colouralpha/-/pipelines
   :alt: Test coverage

.. image:: https://readthedocs.org/projects/colouralpha/badge/?style=flat-square
   :target: https://colouralpha.readthedocs.io/en/stable/
   :alt: Test coverage


Converts and manipulates common color representation (RGB, HSL, web, ...)

This project is forked from `Colour <https://github.com/vaab/colour/>`_.

Features
========

- Damn simple and pythonic way to manipulate color representation (see
  examples below)

- Full conversion between RGB, HSL, 6-digit hex, 3-digit hex, human color

- One object (``Color``) or bunch of single purpose function (``rgb2hex``,
  ``hsl2rgb`` ...)

- ``web`` format that use the smallest representation between
  6-digit (e.g. ``#fa3b2c``), 3-digit (e.g. ``#fbb``), fully spelled
  color (e.g. ``white``), following `W3C color naming`_ for compatible
  CSS or HTML color specifications.

- smooth intuitive color scale generation choosing N color gradients.

- can pick colors for you to identify objects of your application.


.. _W3C color naming: http://www.w3.org/TR/css3-color/#svg-color

Installation and usage instructions can be found in `documentation <https://colouralpha.readthedocs.io/en/stable/>`_.

License
=======

Current license
---------------

Copyright (c) 2022 Marcin Serwin.

Licensed under `BSD License <https://gitlab.com/marcin-serwin/colouralpha/-/blob/main/LICENSE/>`_.

Original license
----------------

The project was forked from `Colour <https://github.com/vaab/colour/>`__.

Copyright (c) 2012-2017 Valentin Lab.

Licensed under the `BSD License <https://gitlab.com/marcin-serwin/colouralpha/-/blob/main/LICENSE.old>`__.
